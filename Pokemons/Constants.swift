//
//  Constants.swift
//  Pokemons
//
//  Created by Łukasz Stoiński on 29.11.2017.
//  Copyright © 2017 Łukasz Stoiński. All rights reserved.
//



struct Pokemon: Codable {
    let forms: [Forms]
    let sprites: Sprites
    
    struct Forms: Codable {
        let name: String
    }
    struct Sprites: Codable {
        let front_default: String
    }
    
}

