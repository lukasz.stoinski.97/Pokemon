//
//  ViewController.swift
//  Pokemons
//
//  Created by Łukasz Stoiński on 29.11.2017.
//  Copyright © 2017 Łukasz Stoiński. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {
    let session = URLSession(configuration: .default)
    var pokemonList = [Pokemon]()
    var pokemonListNames = [String]()
    var pokemonImage: UIImage?
    var correctPokemon = ""
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var leftTopButton: UIButton!
    @IBOutlet weak var leftBotButton: UIButton!
    @IBOutlet weak var rightTopButton: UIButton!
    @IBOutlet weak var rightBotButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newLevels()
    }
    
    func random(max maxNumber: Int) -> Int {
        return Int(arc4random_uniform(UInt32(maxNumber)))
    }
    
    func createLevels() -> [Int] {
        var levels = [Int]()
        while levels.count < 4 {
            let randomNumber = random(max: 801)
            if !(levels.contains(randomNumber))
            {
                levels.append(randomNumber)
            }
        }
        return levels
    }
  
    
  
    
     func newLevels() {
        let levels = createLevels()
        let handlerBlock: (Bool) -> Void = {
            if $0  {
                self.dataReceived()
            }
        }

        
        for number in levels {
            DispatchQueue.main.async {
                self.getPokemonName(level: number, completionHandler: handlerBlock)
            }
        }
    }
    @IBAction func buttonClicked(_ sender: UIButton) {
        if  sender.titleLabel?.text == correctPokemon {
            sender.backgroundColor = .green
        } else {
            sender.backgroundColor = .red

        }
        changeLevels()
    }
    
    func changeLevels() {
        view.isUserInteractionEnabled = false
        pokemonList = []
        pokemonListNames = []
        pokemonImage = nil
        correctPokemon = ""
        newLevels()
    }
    
    func dataReceived() {
        
        if self.pokemonList.count == 4 && pokemonImage != nil {
            DispatchQueue.main.async {
                var listNumbers = [0,1,2,3]
                listNumbers.shuffle()
                print(listNumbers)
                self.leftTopButton.setTitle(self.pokemonListNames[listNumbers[0]], for: .normal)
                self.leftBotButton.setTitle(self.pokemonListNames[listNumbers[1]], for: .normal)
                self.rightBotButton.setTitle(self.pokemonListNames[listNumbers[2]], for: .normal)
                self.rightTopButton.setTitle(self.pokemonListNames[listNumbers[3]], for: .normal)
                self.pokemonImageView.image = self.pokemonImage
                self.view.isUserInteractionEnabled = true
                self.leftBotButton.backgroundColor = .white
                self.leftTopButton.backgroundColor = .white
                self.rightBotButton.backgroundColor = .white
                self.rightTopButton.backgroundColor = .white
                
            }
        }
        
    }

    
    func getPokemonName(level: Int, completionHandler:  @escaping (Bool) -> Void)  {
        
        let url = URL(string: "http://pokeapi.co/api/v2/pokemon/\(level)")!
        session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                "no response"
            }
            
            guard let data = data else { return }
            do {
                //Decode
                let response = try JSONDecoder().decode(Pokemon.self, from: data)
                self.pokemonList.append(response)
                
                for array in response.forms {
                    let pokemonName = array.name
                    self.pokemonListNames.append(pokemonName)
                    break
                }

                if self.pokemonList.count == 1 {
                    let handlerBlock: (Bool) -> Void = {
                        if $0  {
                            self.dataReceived()
                        }
                        
                    }
                    self.getPokemonImage(pokemon: response, completionHandler: handlerBlock)
                    print(response)
                    for array in response.forms {
                        let pokemonName = array.name
                        self.correctPokemon = pokemonName
                        break
                    }
                }
                completionHandler(true)
                
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
        
        }
    
    
    func getPokemonImage(pokemon: Pokemon, completionHandler:  @escaping (Bool) -> Void)   {
        
        let url = URL(string: pokemon.sprites.front_default)
        
        //creating a dataTask
        let getImageFromUrl = session.dataTask(with: url!) { (data, response, error) in
            
            //if there is any error
            if let error = error {
                print(error)
            } else {
                //in case of now error, checking wheather the response is nil or not
                if (response as? HTTPURLResponse) != nil {
                    
                    //checking if the response contains an image
                    if let imageData = data {
                        if let image = UIImage(data: imageData) {
                        self.pokemonImage = image
                        completionHandler(true)

                        }

                    } else {
                        print("Image file is currupted")
                    }
                } else {
                    print("No response from server")
                }
            }
        }
        
        //starting the download task
        getImageFromUrl.resume()
        
    }
}

extension Array {
    mutating func shuffle () {
        for i in (0..<self.count).reversed() {
            let ix1 = i
            let ix2 = Int(arc4random_uniform(UInt32(i+1)))
            (self[ix1], self[ix2]) = (self[ix2], self[ix1])
        }
    }
}


